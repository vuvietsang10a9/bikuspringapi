package com.example.springvsdatabase.controller;

import java.time.LocalDate;
import java.util.Date;

import javax.annotation.security.PermitAll;
import javax.crypto.SecretKey;
import javax.validation.Valid;

import com.example.springvsdatabase.dto.*;
import com.example.springvsdatabase.enumcode.ErrorCode;
import com.example.springvsdatabase.enumcode.SuccessCode;
import com.example.springvsdatabase.jwt.JwtConfig;
import com.example.springvsdatabase.model.Roles;
import com.example.springvsdatabase.model.User;
import com.example.springvsdatabase.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import io.jsonwebtoken.Jwts;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("user")
public class UserController {

    private AuthenticationManager authenticationManager;
    private UserService service;
    private SecretKey secretKey;
    private JwtConfig jwtConfig;
    private PasswordEncoder passwordEncoder;

    @Autowired
    @Lazy
    public UserController(AuthenticationManager authenticationManager, UserService service, SecretKey secretKey,
            JwtConfig jwtConfig, PasswordEncoder passwordEncoder) {
        this.authenticationManager = authenticationManager;
        this.service = service;
        this.secretKey = secretKey;
        this.jwtConfig = jwtConfig;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping("/authenticate")
    @PermitAll
    public ResponseEntity<ResponseDto> login(@Valid @RequestBody LoginDto user) {
        ResponseDto respone = new ResponseDto();
        Authentication authentication = new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword());
        try {
            Authentication authenticate = authenticationManager.authenticate(authentication);
            if (authenticate.isAuthenticated()) {
                User userAuthenticated = service.findUserByUserName(user.getUsername());
                String token = Jwts.builder().setSubject(authenticate.getName())
                        .claim("authorities", authenticate.getAuthorities()).claim("userId", userAuthenticated.getId())
                        .setIssuedAt((new Date())).setExpiration(java.sql.Date.valueOf(LocalDate.now().plusDays(1)))
                        .signWith(secretKey).compact();

                LoginResponseDto loginResponseDto = LoginResponseDto.builder().userId(userAuthenticated.getId())
                        .name(userAuthenticated.getName()).username(userAuthenticated.getUserName())
                        .email(userAuthenticated.getEmail()).role(userAuthenticated.getRole())
                        .token(jwtConfig.getTokenPrefix() + token).build();

                respone.setData(loginResponseDto);
                respone.setSuccessCode(SuccessCode.LOGIN);
                return ResponseEntity.ok().body(respone);
            } else {
                respone.setErrorCode(ErrorCode.LOGIN_FAIL);
                return ResponseEntity.ok().body(respone);
            }
        } catch (Exception e) {
            respone.setErrorCode(ErrorCode.LOGIN_FAIL);
            return ResponseEntity.ok().body(respone);
        }
    }

    @PostMapping("/register")
    public ResponseEntity<ResponseDto> register(@Valid @RequestBody User user) {
        ResponseDto respone = new ResponseDto();
        this.service.register(User.builder().name(user.getName()).userName(user.getUserName()).email(user.getEmail())
                .createDate(LocalDate.now()).password(passwordEncoder.encode(user.getPassword())).status(true)
                .role(Roles.builder().id(4).roleName("USER").build()).build());
        Authentication authentication = new UsernamePasswordAuthenticationToken(user.getUserName(), user.getPassword());
        try {
            Authentication authenticate = authenticationManager.authenticate(authentication);
            if (authenticate.isAuthenticated()) {
                User userAuthenticated = service.findUserByUserName(user.getUserName());

                String token = Jwts.builder().setSubject(authenticate.getName())
                        .claim("authorities", authenticate.getAuthorities()).claim("userId", userAuthenticated.getId())
                        .setIssuedAt((new Date())).setExpiration(java.sql.Date.valueOf(LocalDate.now().plusDays(1)))
                        .signWith(secretKey).compact();

                LoginResponseDto loginResponseDto = LoginResponseDto.builder().userId(userAuthenticated.getId())
                        .name(userAuthenticated.getName()).username(userAuthenticated.getUserName())
                        .email(userAuthenticated.getEmail()).role(userAuthenticated.getRole())
                        .token(jwtConfig.getTokenPrefix() + token).build();

                respone.setData(loginResponseDto);
                respone.setSuccessCode(SuccessCode.SIGN_UP);
                return ResponseEntity.ok().body(respone);
            } else {
                respone.setErrorCode(ErrorCode.SIGN_UP_FAILED);
                return ResponseEntity.ok().body(respone);
            }
        } catch (Exception e) {
            respone.setErrorCode(ErrorCode.SIGN_UP_FAILED);
            return ResponseEntity.ok().body(respone);
        }
    }
    @PostMapping("/admin/register")
    public ResponseEntity<ResponseDto> registerByAdmin(@Valid @RequestBody RegisterUserDto user) {
        ResponseDto respone = new ResponseDto();
        Roles roles =null;
        if(user.getRole().equalsIgnoreCase("ADMIN")){
            roles = Roles.builder().id(1).roleName("ADMIN").build();
        } else if(user.getRole().equalsIgnoreCase("MODIFIER")){
            roles = Roles.builder().id(2).roleName("MODIFIER").build();
        }
        else if(user.getRole().equalsIgnoreCase("SALER")){
            roles = Roles.builder().id(3).roleName("SALER").build();
        }
        else if(user.getRole().equalsIgnoreCase("USER")){
            roles = Roles.builder().id(4).roleName("USER").build();
        }
    try {
      respone.setData(  this.service.register(User.builder().name(user.getName()).userName(user.getUserName()).email(user.getEmail())
                .createDate(LocalDate.now()).password(passwordEncoder.encode(user.getPassword())).status(true)
                .role(roles).build()));
      respone.setSuccessCode(SuccessCode.SIGN_UP);
    }
    catch (Exception e){
        respone.setErrorCode(ErrorCode.SIGN_UP_FAILED);
    }
     return ResponseEntity.ok().body(respone);
    }

    @GetMapping("/users")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ResponseDto> getUsers(@RequestParam int pageNum, @RequestParam int pageSize) {
        ResponseDto responseDto = new ResponseDto();
        try {
            responseDto.setData(this.service.getAllUser(pageNum, pageSize));
            responseDto.setSuccessCode(SuccessCode.GET_ALL_USERS);
            return ResponseEntity.ok().body(responseDto);
        } catch (Exception e) {
            responseDto.setErrorCode(ErrorCode.GET_ALL_USERS);
            return ResponseEntity.ok().body(responseDto);
        }
    }
    @GetMapping("/view/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ResponseDto> getUserById(@PathVariable("id") int userId) {
        ResponseDto responseDto = new ResponseDto();
        try {
            responseDto.setData(this.service.getUserById(userId));
            responseDto.setSuccessCode(SuccessCode.GET_USER);
            return ResponseEntity.ok().body(responseDto);
        } catch (Exception e) {
            responseDto.setErrorCode(ErrorCode.GET_USER);
            return ResponseEntity.ok().body(responseDto);
        }
    }

    @PutMapping("/activate/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ResponseDto> activateUser(@PathVariable("id") int userId, @RequestParam boolean status) {
        ResponseDto responseDto = new ResponseDto();
        try {
            responseDto.setData(this.service.activateUser(userId, status));
            responseDto.setSuccessCode(SuccessCode.ACTIVATE_USER);
            return ResponseEntity.ok().body(responseDto);
        } catch (Exception e) {
            responseDto.setErrorCode(ErrorCode.ACTIVATE_USER);
            return ResponseEntity.ok().body(responseDto);
        }
    }

    @PutMapping("/update/{id}")
    @PreAuthorize("hasRole('ADMIN')")

    public ResponseEntity<ResponseDto> updateUser(@PathVariable("id") int userId, @RequestBody UpdateUserDto user) {
        ResponseDto responseDto = new ResponseDto();
        try {
            if (this.service.updateUser(user, userId) == false) {
                responseDto.setErrorCode(ErrorCode.UPDATE_USER);
                return ResponseEntity.badRequest().build();
            } else {
                responseDto.setData(this.service.updateUser(user, userId));
                responseDto.setSuccessCode(SuccessCode.UPDATE_USER);
                return ResponseEntity.ok().body(responseDto);
            }
        } catch (Exception e) {
            responseDto.setErrorCode(ErrorCode.UPDATE_USER);
            return ResponseEntity.ok().body(responseDto);
        }
    }
    @DeleteMapping("/deleteUser/{id}")
    @PreAuthorize("hasRole('ADMIN')")

    public ResponseEntity<ResponseDto> deleteUser(@PathVariable("id") int userId) {
        ResponseDto responseDto = new ResponseDto();
            if (this.service.deleteUserById(userId) == false) {
                responseDto.setErrorCode(ErrorCode.DELETE_USER_FAIL);
                return ResponseEntity.badRequest().build();
            } else {
                responseDto.setData(this.service.deleteUserById(userId));
                responseDto.setSuccessCode(SuccessCode.DELETE_USER_SUCCESSFULLY);
                return ResponseEntity.ok().body(responseDto);
            }
    }
}
