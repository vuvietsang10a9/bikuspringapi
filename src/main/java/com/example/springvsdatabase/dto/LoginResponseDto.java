package com.example.springvsdatabase.dto;

import com.example.springvsdatabase.model.Roles;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoginResponseDto {
    private int userId;
    private String name;
    private String username;
    private String email;
    private Roles role;
    private String token;
}
