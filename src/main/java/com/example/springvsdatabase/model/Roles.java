package com.example.springvsdatabase.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Builder;

@Entity
@Builder
public class Roles {
    @Id
    private int id;
    private String roleName;
    private LocalDate createDate;

    @JsonIgnore
    @OneToMany(mappedBy = "role")
    private List<User> users = new ArrayList<>();

    public Roles() {
    }

    public Roles(int id, String roleName, LocalDate createDate, List<User> users) {
        this.id = id;
        this.roleName = roleName;
        this.createDate = createDate;
        this.users = users;
    }

    public Roles(int id, String roleName, LocalDate createDate) {
        this.id = id;
        this.roleName = roleName;
        this.createDate = createDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

}
