package com.example.springvsdatabase.repository;

import com.example.springvsdatabase.model.Products;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Products, Integer> {
    Page<Products> findProductsByName(Pageable pageable, String productName);

    Page<Products> findProductsByMaker(Pageable pageable, String maker);

    Page<Products> findProductsByPriceBetween(Pageable pageable, double min, double max);
}
