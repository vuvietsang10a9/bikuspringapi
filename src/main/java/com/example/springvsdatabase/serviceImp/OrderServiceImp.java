package com.example.springvsdatabase.serviceImp;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.example.springvsdatabase.dto.OrderDetailsDto;
import com.example.springvsdatabase.model.OrderDetails;
import com.example.springvsdatabase.model.Orders;
import com.example.springvsdatabase.model.Products;
import com.example.springvsdatabase.model.User;
import com.example.springvsdatabase.repository.OrderDetailsRepository;
import com.example.springvsdatabase.repository.OrdersRepository;
import com.example.springvsdatabase.repository.ProductRepository;
import com.example.springvsdatabase.repository.UserRepository;
import com.example.springvsdatabase.services.OrdersService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImp implements OrdersService {

    @Autowired
    private OrderDetailsRepository orderDetailsRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private OrdersRepository repo;
    @Autowired
    private ProductRepository productRepository;

    @Override
    public Orders getOrder(int orderId) {
        return this.repo.findById(orderId).get();
    }

    @Override
    public boolean deleteOrder(int orderId) {

        try {
            Orders order = this.repo.findById(orderId).get();
            order.setStatus(false);
            this.repo.save(order);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean updateOrder(Orders order, int orderId) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean confirmOrder(int orderId) {
        try {
            Orders order = this.repo.findById(orderId).get();
            order.setStatus(true);
            this.repo.save(order);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Page<Orders> getAllOrders(int pageNum, int pageSize) {
        Pageable pagable = PageRequest.of(pageNum, pageSize);
        Page<Orders> pageOrder = this.repo.findAll(pagable);
        return pageOrder;
    }

    @Override
    public Page<Orders> getOrderByUserId(int pageNum, int pageSize, int userId) {
        Pageable pagable = PageRequest.of(pageNum, pageSize);
        Page<Orders> pageOrder = this.repo.findByUserId(pagable,userId);
        return pageOrder;
    }

    @Override
    public List<Orders> getOrdersByEmail(String email) {
        return this.repo.getOrdersByUserEmail(email);
    }

    @Override
    public boolean checkout(OrderDetailsDto[] orderDetails) {
        LocalDate date = LocalDate.now();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String userName = (String) authentication.getPrincipal();
        User userInDb = this.userRepository.findUserByUserName(userName);
        try {
            Orders order = new Orders(false, date, userInDb);
            this.repo.save(order);
            for (int i = 0; i < orderDetails.length; i++) {
                Products product = this.productRepository
                        .findById(Integer.parseInt(orderDetails[i].getProductId())).get();
                int quantity = orderDetails[i].getQuantity();
                double price = quantity * product.getPrice();
                OrderDetails oDetails = OrderDetails.builder().order(order).product(product).quantity(quantity)
                        .price(price).build();
                this.orderDetailsRepository.save(oDetails);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}
