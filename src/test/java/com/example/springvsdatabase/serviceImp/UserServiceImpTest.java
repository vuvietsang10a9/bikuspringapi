package com.example.springvsdatabase.serviceImp;

import com.example.springvsdatabase.model.Roles;
import com.example.springvsdatabase.model.User;
import com.example.springvsdatabase.repository.UserRepository;
import com.google.common.base.Verify;
import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

@ExtendWith(MockitoExtension.class)
class UserServiceImpTest {

    @Mock private UserRepository userRepository;
    private UserServiceImp undertest;


    @BeforeEach
    void setUp(){
        undertest = new UserServiceImp(userRepository);
    }

    @Test
    void getAllUser(){
        undertest.getAllUser2();
        Mockito.verify(userRepository).findAll();
    }

    @Test
    void canAddUser(){
        //given
        User user = User.builder().id(1).userName("sang").name("sang")
                .email("vuvietsang10a9@gmail.com")
                .role(Roles.builder().roleName("ADMIN").id(1).build()).status(true).createDate(LocalDate.now()).password("1234566789").build();
//        when
        undertest.register(user);

        //then
        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        Mockito.verify(userRepository).save(userArgumentCaptor.capture());

        User capturedUser = userArgumentCaptor.getValue();
        AssertionsForClassTypes.assertThat(capturedUser).isEqualTo(user);
    }
}
